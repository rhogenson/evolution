module Main where

import qualified BrainFuck
import System.Environment (getArgs)
import qualified Evolve
import qualified Control.Concurrent.Chan as Chan
import Control.Concurrent (forkIO)
import Data.List (intercalate)

type BF = [BrainFuck.BF]

main :: IO ()
main = do a <- getArgs
          s <- readFile (head a)
          let bf = BrainFuck.strToBF s
          c <- Chan.newChan
          _ <- forkIO $ Evolve.evolve c bf
          generations <- Chan.getChanContents c
          mapM_ display generations
  where display e = putStrLn $ BrainFuck.runBF e ++ "\ncode: (" ++ intercalate "" (map show e) ++ ")"
