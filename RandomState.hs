module RandomState (splitG, splits) where

import qualified Control.Monad.State as State
import qualified System.Random as Random
import Data.List (unfoldr)

splitG :: Random.RandomGen g => State.State g g
splitG = do g <- State.get
            let (g1, g2) = Random.split g
            State.put g1
            return g2

splits :: Random.RandomGen g => State.State g [g]
splits = unfoldr (Just . Random.split) <$> splitG
